# My component tester

## Installation

Run `npm install && bower install`

## Gulp tasks

This component tester is basically built on top of these gulp commands, its a workflow based on it.

`gulp new-element --name [NAME]` This creates an element and adds it into your 'book' of elements

`gulp remove-element --name [NAME]` This removes an element, if it can match the name flag

`gulp export-element --name [NAME]` Exports the element, if matched, into the export folder

For development use

`gulp serve`

## Tests

Tests are written in the elements/[ELEMENT]/test folder and are run with WCT