var extend = require('util')._extend;
var path = require('path');

var config = {};

config.path = {
  elements:     'elements',
  elementsDist: 'elements',
  bower:        'bower_components',
  demo:         'demo',
  export:       'exports'
};

module.exports = config;