

var AUTOPREFIXER_BROWSERS = [
  'ie >= 10',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios >= 7',
  'android >= 4.4',
  'bb >= 10'
];

var createStyleModulePrepend = function(name) {
  return '\n' +
      '<dom-module id="' + name + '">' +
      '\n  ' +
      '<template>' +
      '\n    ' +
      '<style>' +
      '\n';
};

var createStyleModuleAppend = function() {
  return '\n    ' +
      '</style>' +
      '\n  ' +
      '</template>' +
      '\n' +
      '</dom-module>';
};

// Elements style modules (inline into head - for initial page load)
// Uncompressed, autoprefixed // development
gulp.task('sass:elements:dev', function() {
  var srcFiles = matchFiles(paths.elements, true, 'scss');

  var sassOptions = {
    outputStyle: 'expanded'
  };

  return gulp.src(srcFiles)
      .pipe($.sass(sassOptions).on('error', $.sass.logError))
      .pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
      // Wrap css into a Polymer Style Module
      .pipe($.insert.transform(function(contents, file) {
        var pathSplit = file.path.split('/');
        var name = pathSplit[pathSplit.length - 1].split('.')[0];

        var prepend = createStyleModulePrepend(name);
        var append = createStyleModuleAppend();
        return prepend + contents + append;
      }))
      .pipe($.rename({extname: '.html'}))
      .pipe(gulp.dest(paths.elements))
      .pipe($.size({title: 'Elements styles'}));
});