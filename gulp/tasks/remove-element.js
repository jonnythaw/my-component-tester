require('../utils/requires.js');



// Removes the element
// -------
gulp.task('remove-element', function(cb) {
  if (!$.util.env.name) {
    $.util.log($.util.colors.red(
        'Error: Please specify the name of the component via the --name flag'));
    return;
  }

  var elName = $.util.env.name.toLowerCase()
      .split(' ').join('-').split('_').join('-');

  try {
    var file = fs.statSync(path.join(paths.elementsDist,elName,elName + '.html'));

    runSequence(
      'clean:remove-element',
      'replace:remove-element',
      cb);
  }
  catch (e) {
    $.util.log($.util.colors.red(
        'Error: This element doesn\'t seem to exist'));
    return;
  }
});

gulp.task('replace:remove-element', function() {
  var elName = $.util.env.name.toLowerCase()
      .split(' ').join('-').split('_').join('-');

  return gulp.src(path.join(paths.elements,'index.html'), {base: "./"})
      .pipe($.replace('\n<link rel="import" href="' + path.join(elName,elName + '.html') + '">', ''))
      .pipe(gulp.dest('./'));
});

gulp.task('clean:remove-element', function() {
  var elName = $.util.env.name.toLowerCase()
      .split(' ').join('-').split('_').join('-');

  return del([
    path.join(paths.elements, elName),
    path.join(paths.elementsDist, elName)
  ]);
});

