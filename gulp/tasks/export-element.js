require('../utils/requires.js');

/**
 * This files task is commanded by the export-element task
 * 
 * Run it as `gulp export-element --name [element-name]`
 *
 * It exports your element (if matched) into the exports folder 
 * and sets up the structure to mirror the seed element for
 * distribution
 */



/**
 * The entry gulp task
 * --------
 */
gulp.task('export-element', function(cb) {
  if (!$.util.env.name) {
    $.util.log($.util.colors.red(
        'Error: Please specify the name of the component via the --name flag'));
    return;
  }
  else {
    var elName = $.util.env.name.toLowerCase()
        .split(' ').join('-').split('_').join('-');

    // If the element exists, run the gulp tasks else exit
    try {
      var file = fs.statSync(path.join(paths.elementsDist,elName,elName + '.html'));

      runSequence(
        // Copy root files to .tmp
        'sass:tab-css',
        'sass:inline-css',
        'sass:clean-css',
        'clean-comments',
        'clean-dependencies',
        'copy:move-demo',
        'copy:move-tests',
        'copy:keep-files',
        'create-git',
        cb);
    }
    catch (e) {
      $.util.log($.util.colors.red(
          'Error: This element doesn\'t seem to exist'));
      return;
    }
  }
});


/**
 * Fixes the indentation ahead of inlining the css
 * --------
 */
gulp.task('sass:tab-css', function() {
  var elName = $.util.env.name.toLowerCase()
      .split(' ').join('-').split('_').join('-');

  return gulp.src(path.join(paths.elementsDist, elName, elName + '-styles.html'))
    .pipe($.indent({tabs:false, amount:4 }))
    .pipe(gulp.dest(path.join(paths.export, elName)));
});


/**
 * Inlines the css from the -styles.html
 * --------
 */
gulp.task('sass:inline-css', function() {
  var elName = $.util.env.name.toLowerCase()
      .split(' ').join('-').split('_').join('-');

  var styles = fs.readFileSync(path.join(paths.export, elName, elName+'-styles.html'), "utf8"),
      strippedStyles = '<style>\n' + 
                          styles.replace(/(<([^>]+)>)/ig,"").replace(/^\s*[\r\n]/gm,"") +
                        '</style>';

  return gulp.src([
      path.join(paths.elementsDist,elName,'*.html'),
      '!'+path.join(paths.elementsDist,elName,elName + '-demo.html'),
      '!'+path.join(paths.elementsDist,elName,elName + '-styles.html')
    ])
    .pipe($.replace('<style include="' + elName + '-styles"></style>', strippedStyles))
    .pipe(gulp.dest(path.join(paths.export, elName)));
});


/**
 * Deletes the -styles.html from the export folder
 * --------
 */
gulp.task('sass:clean-css', function() {
  var elName = $.util.env.name.toLowerCase()
      .split(' ').join('-').split('_').join('-');

  return del([
    path.join(paths.export, elName, elName + '-styles.html')
  ]);
});


/**
 * Realigns the comments that dictate hero and demo
 * so that it matches the seed element folder structure
 * --------
 */
gulp.task('clean-comments', function() {
  var elName = $.util.env.name.toLowerCase()
      .split(' ').join('-').split('_').join('-'),
      dest = path.join(paths.export, elName);

  return gulp.src([
      path.join(paths.elementsDist,elName,'*.html'),
      '!'+path.join(paths.elementsDist,elName,elName + '-demo.html'),
      '!'+path.join(paths.elementsDist,elName,elName + '-styles.html')
    ], { base: path.join('./', paths.export, elName) })
    .pipe($.changed(dest))
    .pipe($.replace('@hero hero.svg\n', ''))
    .pipe($.replace('@demo elements/' + elName + '/' + elName + '-demo.html', '@demo demo/index.html'))
    .pipe(gulp.dest(dest));
});


/**
 * Fixes all the levels of the imports in your element
 * --------
 */
gulp.task('clean-dependencies', function() {
  var elName = $.util.env.name.toLowerCase()
      .split(' ').join('-').split('_').join('-'),
      dest = path.join(paths.export, elName);

  return gulp.src([
      path.join(paths.export,elName,'*.html'),
      '!'+path.join(paths.export,elName,elName + '-demo.html'),
      '!'+path.join(paths.export,elName,elName + '-styles.html')
    ], { base: path.join('./', paths.export, elName) })
    // .pipe($.changed(dest))
    .pipe($.replace('../../bower_components', '..'))
    .pipe($.replace('<link rel="import" href="' + elName + '-styles.html">',''))
    .pipe(gulp.dest(dest));
});


/**
 * Moves the demo from the element folder into a
 * /demo folder and fixes the levels of imports
 * --------
 */
gulp.task('copy:move-demo', function() {
  var elName = $.util.env.name.toLowerCase()
      .split(' ').join('-').split('_').join('-'),
      dest = path.join(paths.export, elName, 'demo');

  return gulp.src(path.join(paths.elementsDist, elName, elName + '-demo.html'))
    // .pipe($.changed(dest))
    .pipe($.rename(function (path) {
      path.basename = "index";
    }))
    .pipe($.replace("../../bower_components/webcomponentsjs/webcomponents-lite.min.js","../../webcomponentsjs/webcomponents-lite.min.js"))
    .pipe($.replace('<link rel="import" href="' + elName + '.html">', '<link rel="import" href="../' + elName + '.html">'))
    .pipe(gulp.dest(dest));
});


/**
 * Moves the tests from the element folder into a
 * /test folder and fixes the levels of imports
 * --------
 */
gulp.task('copy:move-tests', function() {
  var elName = $.util.env.name.toLowerCase()
      .split(' ').join('-').split('_').join('-'),
      dest = path.join(paths.export, elName, 'test');

  return gulp.src(path.join(paths.elementsDist, elName, 'test', '*.html'))
    // .pipe($.changed(dest))
    .pipe($.replace("../../bower_components/","../../"))
    .pipe(gulp.dest(dest));
});


/**
 * Moves over important files in their same state
 * into the exported folder
 * --------
 */
gulp.task('copy:keep-files', function() {
  var elName = $.util.env.name.toLowerCase()
      .split(' ').join('-').split('_').join('-');

  var seedEl = 'seed-element',
      dest = path.join(paths.export, elName);

  return gulp.src([
      path.join(paths.elementsDist, elName, 'README.md'),
      path.join(paths.elementsDist, elName, 'bower.json'),
      path.join(paths.bower, seedEl, 'index.html')
    ])
    // .pipe($.changed(dest))
    .pipe(gulp.dest(dest));
});


/**
 * Creates a git repository inside your newly exported element
 * --------
 */
gulp.task('create-git', function(cb) {
  var elName = $.util.env.name.toLowerCase()
      .split(' ').join('-').split('_').join('-');

  try {
    var file = fs.statSync(path.join(paths.export,elName,'.git'));
    cb();
  }
  catch (e) {
    if (e.code === 'ENOENT') {
      try {
        process.chdir(path.join(paths.export, elName));

        $.git.init(function(err) {
          if(err) throw err;

          cb();
        });
      }
      catch (err) {
        console.log('chdir: ' + err);
      }
    }
  }
});

