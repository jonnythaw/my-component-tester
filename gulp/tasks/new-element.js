require('../utils/requires.js');



/**
 * The entry gulp task
 * --------
 */
gulp.task('new-element', function(cb) {
  if (!$.util.env.name) {
    $.util.log($.util.colors.red(
        'Error: Please specify the name of the component via the --name flag'));
    return;
  }
  runSequence(
    'create-index',
    'copy:seed-element',
    'copy:seed-files',
    'create-styles',
    'append:new-element',
    'sass:elements:dev',
    'create-demo',
    'create-readme',
    'create-tests',
    cb);
});


/**
 * In case the index of element imports doesnt exist
 * create it
 * --------
 */
gulp.task('create-index', function(cb) {
  try {
    var file = fs.statSync(path.join(paths.elements,'index.html'));
    cb();
  }
  catch (e) {
    if (e.code === 'ENOENT') {
      return $.file('index.html', '', {src: true})
        .pipe(gulp.dest(path.join(paths.elements)));
    }
    else {
      cb();
    }
  }
});


/**
 * The task that copies the seed element
 * from our locally installed bower
 * components
 * --------
 */
gulp.task('copy:seed-element', function() {
  // Normalizing element name (no spaces, no underscores, all lowercase);
  var elName = $.util.env.name.toLowerCase()
                  .split(' ').join('-').split('_').join('-');
  var destFld = path.join(paths.elements, elName);

  var seedEl = 'seed-element';

  $.util.log('Creating element:\n- ' +
      $.util.colors.green(destFld + '/' + elName + '.html') + ' (markup)\n- ' +
      $.util.colors.green(destFld + '/' + elName + '-styles.scss') +
      ' (styles)');

  return gulp.src(path.join(paths.bower, seedEl, seedEl + '.html'))
      // Remove license (2 steps of replace)
      .pipe($.replace(/<!--(\n|\r|\s)*@license[^>]*/g, ''))
      .pipe($.replace(/^>[\n]*/g, ''))
      // Replace 'seed-element' with the new element's name
      .pipe($.replace(seedEl, elName))
      // Fix the level of Polymer in the element
      .pipe($.replace('../polymer', '../../bower_components/polymer'))
      // Replace demo tag
      .pipe($.replace('@demo demo/index.html', '@demo ' + path.join(paths.elementsDist, elName, elName + '-demo.html')))
      // Repace inline style with import to external styles
      .pipe($.replace(/polymer\.html\">/g, 'polymer.html">\n' +
          '<link rel="import" href="' + elName + '-styles.html">'))
      .pipe($.replace(/<style>[\w\W]*<\/style>/g,
          '<style include="' + elName + '-styles"></style>'))
      // Replace the properties that are automatically added
      .pipe($.replace(/properties: {[\w\W]* \/\/ Element Lifecycle/g,
          'properties: {\n\n    ' +
          '  },'))
      // Remove the default methods
      .pipe($.replace(/  \/\/ Element Behavior[\w\W]* }\);/g,
          '  foo: function() {\n        ' +
            'return \'bar\';\n      ' +
          '}\n  ' +
          '});'))
      // Remove unecessary markup
      .pipe($.replace(/<p[\w\W]*<\/p>/g,
          ''))
      // Rename file
      .pipe($.rename(elName + '.html'))
      // Copy to out folder
      .pipe(gulp.dest(destFld));
});


/**
 * Copies any necessary files from the seed element
 * --------
 */
gulp.task('copy:seed-files', function() {
  // Normalizing element name (no spaces, no underscores, all lowercase);
  var elName = $.util.env.name.toLowerCase()
                  .split(' ').join('-').split('_').join('-');
  var destFld = path.join(paths.elements, elName);

  var seedEl = 'seed-element';

  return gulp.src([path.join(paths.bower, seedEl, 'bower.json')])
      .pipe($.replace(seedEl, elName))
      .pipe(gulp.dest(destFld));
});


/**
 * Creates the seperate .scss file
 * for the element
 * --------
 */
gulp.task('create-styles', function() {
  var elName = $.util.env.name.toLowerCase()
      .split(' ').join('-').split('_').join('-');
  var destFld = path.join(paths.elements, elName);

  var str =
  '@import \'../../sass/base\';\n\n' +
  ':host {\n' +
  '  display: block;\n' +
  '}\n';

  return $.file(elName + '-styles.scss', str, {src: true})
      .pipe(gulp.dest(destFld));
});


/**
 * Appends the new element to elements index
 * --------
 */
gulp.task('append:new-element', function() {
  var elName = $.util.env.name.toLowerCase()
      .split(' ').join('-').split('_').join('-');

  return gulp.src(paths.elements+'/index.html', {base: "./"})
      .pipe($.insert.append('\n<link rel="import" href="' + path.join(elName,elName + '.html') + '">'))
      .pipe(gulp.dest('./'));
});


/**
 * Creates a demo file
 * --------
 */
gulp.task('create-demo', function() {
  var elName = $.util.env.name.toLowerCase()
      .split(' ').join('-').split('_').join('-');
  var destFld = path.join(paths.elements, elName);

  var str = '<!doctype html>'+'\n'+
            ' <html>'+'\n'+
            '   <head>'+'\n'+
            '     <meta charset="utf-8">'+'\n'+
            '     <meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=yes">'+'\n'+
            '     <title>' + elName + ' Demo</title>'+'\n'+
            '     <script src="../../bower_components/webcomponentsjs/webcomponents-lite.min.js"></script>'+'\n'+
            '     <link rel="import" href="' + elName + '.html' + '">'+'\n'+
            '   </head>'+'\n'+
            '   <body unresolved>'+'\n'+
            '     <p>An example of <code>&lt;'+elName+'&gt;</code>:</p>'+'\n'+
            '     <'+elName+'></'+elName+'>'+'\n\n'+
            '     <script>'+'\n'+
            '     \n'+
            '     </script>'+'\n'+
            '   </body>'+'\n'+
            ' </html>';

  return $.file(elName + '-demo.html', str, {src: true})
      .pipe(gulp.dest(destFld));
});


/**
 * Creates a readme
 * --------
 */
gulp.task('create-readme', function() {
  var elName = $.util.env.name.toLowerCase()
      .split(' ').join('-').split('_').join('-');
  var destFld = path.join(paths.elements, elName);

  var str = elName + '\n' +
            '====='+'\n\n'+
            'A smashing new element';

  return $.file('README.md', str, {src: true})
      .pipe(gulp.dest(destFld));
});


// I am full aware at how mental this is
// Maybe I should create a seperate repo with these dumb files in
// and copy them over?

gulp.task('create-tests', function() {
  var elName = $.util.env.name.toLowerCase()
      .split(' ').join('-').split('_').join('-');

  var destFld = path.join(paths.elements, elName, 'test');

  var indexStr = '<!DOCTYPE html>\n' +
                 '<html>\n' +
                 '  <head>\n' +
                 '     <meta charset="utf-8">\n' +
                 '     <meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=yes">\n' +
                 '     \n' +
                 '     <script src="../../bower_components/webcomponentsjs/webcomponents-lite.js"></script>\n' +
                 '     <script src="../../bower_components/web-component-tester/browser.js"></script>\n' +
                 '  </head>\n' +
                 '  <body>\n' +
                 '     <script>\n' +
                 '       // Load and run all tests (.html, .js):\n' +
                 '       WCT.loadSuites([\n' +
                 '         \'basic-test.html\',\n' +
                 '         \'basic-test.html?dom=shadow\'\n' +
                 '       ]);\n' +
                 '     </script>\n' +
                 '     \n' +
                 '  </body>\n' +
                 '</html>';

  var stream1 = $.file('index.html', indexStr, {src: true})
      .pipe(gulp.dest(destFld));

  var basicStr = '<!doctype html>\n' +
            ' <html>\n' +
            ' <head>\n' +
            '    <meta charset="utf-8">\n' +
            '    \n' +
            '    <script src="../../bower_components/webcomponentsjs/webcomponents.min.js"></script>\n' +
            '    <script src="../../bower_components/web-component-tester/browser.js"></script>\n' +
            '    <script src="../../bower_components/test-fixture/test-fixture-mocha.js"></script>\n' +
            '    \n' +
            '    <link rel="import" href="../' + elName + '.html">\n' +
            '    <link rel="import" href="../../bower_components/test-fixture/test-fixture.html">\n' +
            '   \n' +
            ' </head>\n' +
            ' <body>\n' +
            '   \n' +
            '    <test-fixture id="test">\n' +
            '      <template>\n' +
            '        <' + elName + '></' + elName + '>\n' +
            '      </template>\n' +
            '    </test-fixture>\n' +
            '    \n' +
            '    <script>\n' +
            '      describe(\'<' + elName + '>\', function() {\n' +
            '        var test;\n' +
            '        \n' +
            '        beforeEach(function() {\n' +
            '          test = fixture(\'test\');\n' +
            '        });\n' +
            '        \n' +
            '        context(\'common tests\', function() {\n' +
            '        \n' +
            '        });\n' +
            '      });\n' +
            '    </script>\n' +
            ' </body>\n' +
            ' </html>';


  var stream2 = $.file('basic-test.html', basicStr, {src: true})
      .pipe(gulp.dest(destFld));

  return merge(stream1, stream2);
});
