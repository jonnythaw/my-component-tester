require('../utils/requires.js');

gulp.task('sass:lint', function() {

  return gulp.src([
        matchFiles(paths.elements, true, 'scss')
      ])
      .pipe($.scssLint({'config': '.scsslintrc.yml'}));
});