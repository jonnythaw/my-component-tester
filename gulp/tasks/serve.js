var browserSyncPrefix = packageJson.name || 'Polymer App';
var browserSyncSnipperOpts = {
  rule: {
    match: '<span id="browser-sync-binding"></span>',
    fn: function(snippet) {
      return snippet;
    }
  }
};

// Watch files for changes & reload
gulp.task('serve:browser', function() {
  browserSync({
    port: 5000,
    notify: false,
    logPrefix: browserSyncPrefix,
    snippetOptions: browserSyncSnipperOpts,
    // Run as an https by uncommenting 'https: true'
    // Note: this uses an unsigned certificate which on first access
    //       will present a certificate warning in the browser.
    // https: true,
    server: {
      middleware: [historyApiFallback()],
      routes: {
        '/bower_components': paths.bower
      }
    }
  });
});

gulp.task('serve', ['serve:browser', 'watch']);

gulp.task('watch', [], function(){
  var matchElementsSass = matchFiles(paths.elements, true, 'scss');
  gulp.watch([matchElementsSass], ['watch:elementsSass']);

  var matchElementsHtml = matchFiles(paths.elements, true, 'html');
  gulp.watch([matchElementsHtml], { debounceDelay: 1000 }, ['logging', reload]);
});

gulp.task('logging', function() {
  $.util.colors.green('Reloading browser...');
  return;
});