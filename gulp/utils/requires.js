// Config and utils
global.packageJson = require('../../package.json');
global.config = require('../../config.js');
global.paths = global.config.path;
global.matchFiles = require('./matchFiles.js');

// Gulp and other npm packages
global.gulp = require('gulp');
global.$ = require('gulp-load-plugins')();
global.path = require('path');
global.runSequence = require('run-sequence');
global.fs = require('fs');
global.merge = require('merge-stream');
global.del = require('del');
global.browserSync = require('browser-sync');
global.reload = global.browserSync.reload;
global.historyApiFallback = require('connect-history-api-fallback');