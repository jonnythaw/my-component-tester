var path = require('path');

var matchFiles = function(folder, includeSubfolders, extension) {
  var file = '*';
  file += extension ? '.' + extension : '';

  return includeSubfolders ?
      path.join(folder, '**', file) : path.join(folder, file);
};

module.exports = matchFiles;
